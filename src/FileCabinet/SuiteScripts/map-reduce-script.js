/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 */
 
 define(['N/file', 'N/log'], 
 	function(file, log){
 		
 		function getInputData(){
 			// get the CSV file
 			var csvFile = file.load({
 				id: 12345678 // the actual file id
 			});

 			return csvFile;
 		}

 		function map(context){
 			if(context.key > 0){ // the CSV file has a header row, so let's skip it

                // get the columns
 				var csvRow = context.value.split(',');

 				context.write({
					key: csvRow[0],
					value: csvRow[1]
				});	
 			}	
 		}

 		function summarize(summary){
 			var totals = 0;

 			summary.output.iterator().each(function(key, value){
 				log.debug('output', key + '|' + value)
 				totals += parseFloat(value);
 				return true;
 			});

 			log.debug('Sum of totals', totals);
 		}

 		return{
 			getInputData: getInputData,
 			map: map,
 			summarize: summarize
 		}
 	})