/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 */

define(['N/record', 'N/log'], 
	function(record, log){
		function validateLine(context){

			var currentRecord = context.currentRecord;
			var sublistName = context.sublistId;

			if(sublistName === 'inventory'){
				// get quantity being transferred
				var quantityToTransfer = currentRecord.getCurrentSublistValue({
					sublistId: 'inventory',
					fieldId: 'adjustqtyby'
				})

				// get the FROM location
				var fromLocationId = currentRecord.getValue({
					fieldId: 'location'
				})

				// get item ID
				var itemId = currentRecord.getCurrentSublistValue({
					sublistId: 'inventory',
					fieldId: 'item'
				})

				// load item record
				var itemRec = record.load({
					type: record.Type.INVENTORY_ITEM,
					id: itemId
				})

				// lookup available quantity at location and compare with the quantity requested
				var noOfLocations = itemRec.getLineCount({
					sublistId: 'locations'
				});

				var enoughQtyForTransfer = true;

				for (var i=0; i<noOfLocations; i++){
					var locationId = itemRec.getSublistValue({
						sublistId: 'locations',
						fieldId: 'location',
						line: i
					});

					if(fromLocationId == locationId){
						// get quantityavailable
						var quantityAvailable = itemRec.getSublistValue({
							sublistId: 'locations',
							fieldId: 'quantityavailable',
							line: i
						})

						if(quantityToTransfer >= quantityAvailable) {
							enoughQtyForTransfer = false
							break;
						}
					}
				}

				// if qty is available
				if(enoughQtyForTransfer) return true
				else {
					// else qty is not sufficient, prevent from continuing
					alert('Insufficient quantities available at original location');
					return false
				}
			} else {
				return true;
			}
		}

		return {
			validateLine: validateLine
		}
	})