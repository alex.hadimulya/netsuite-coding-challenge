/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 */

define(['N/task', 'N/runtime', 'N/log'], 
	function(task, runtime, log){
		function execute(context){

			// only run on scheduled invocations
			if(context.type !== context.InvocationType.SCHEDULED) return;

			// get the search ID parameter from the deployment record
			var mySearchId = runtime.getCurrentScript().getParameter('custscript_savedsearchid');

			// create the search task
			var scheduledSearchTask = task.create({
				taskType: task.TaskType.SEARCH,
				filePath: '/some-folder/file.csv',
				savedSearchId: mySearchId
			});

			var scheduledSearchTaskId = scheduledSearchTask.submit();

		}

		return{
			execute: execute
		};

	})