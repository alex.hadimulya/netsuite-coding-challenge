/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 */
define(['N/record', 'N/log'],

    function(record, log) {

        function afterSubmit(context) {
        	var newFulfillmentRec = context.newRecord;

        	// get the sales order
        	var salesOrderId = newFulfillmentRec.getValue({
        		fieldId: 'createdfrom'
        	});

            try{

            	// transform the sales order to invoice
            	// might also need to check if TERMS is set? Transform to INVOICE vs CASHSALE
            	var invoiceRec = record.transform({
            		fromType: record.Type.SALES_ORDER,
            		fromId: salesOrderId,
            		toType: record.Type.INVOICE,
            		//isDynamic: true
            	});

            	// get the line items on the invoice
            	var noOfItems = invoiceRec.getLineCount({
            		sublistId: 'item'
            	});

            	// check which ones have not been fulfilled yet
            	for(var i=0;i<noOfItems;i++){
            		var orderedQty = invoiceRec.getSublistValue({
            			sublistId: 'item',
            			fieldId: 'quantity',
            			line: i
            		});

            		var remainingQty = invoiceRec.getSublistValue({
            			sublistId: 'item',
            			fieldId: 'quantityremaining',
            			line: i
            		});

            		// update the line item qty so only the fulfilled qty is invoiced
            		invoiceRec.setSublistValue({
            			sublistId: 'item',
            			fieldId: 'quantity',
            			line: i,
            			value: orderedQty-remainingQty
            		});

            	}

            	invoiceRec.save();

            }catch(e){
                log.debug({
                    title: 'ERROR',
                    details: e.name + ' | ' + e.message
                });
            }
        }

        return {
            afterSubmit: afterSubmit
        };

    });