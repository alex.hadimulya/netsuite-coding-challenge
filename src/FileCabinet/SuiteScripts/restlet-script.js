/**
 * @NApiVersion 2.x
 * @NScriptType restlet
 */
 
define(['N/record', 'N/search', 'N/log'], 
	function(record, search, log){
		function _get(context){
			
			// get an HTS record via hts8_code passed in the get parameter
			var htsRecordSearch = search.create({
				type: 'customrecord_hts',
				filters: [{name: 'custrecord_hts8', operator: 'is', values: context.hts8_code}],
				columns: ['custrecord_hts8', 'custrecord_brief_description', 'custrecord_mfn_ad_val_rate', 'custrecord_mfn_specific_rate']
			});

			var results = htsRecordSearch.run();

			var result = results.getRange({
				start: 0,
				end: 1
			})[0];

			if(result){
				// if found, return the data as JSON
				var resultJsonObj = {
					hts8_code: result.getValue('custrecord_hts8'),
					brief_description: result.getValue('custrecord_brief_description'),
					mfn_ad_val_rate: result.getValue('custrecord_mfn_ad_val_rate'),
					mfn_specific_rate: result.getValue('custrecord_mfn_specific_rate')
				}
			} else {
				var resultJsonObj = {
					error: 'no record found with that hts8_code'
				}
			}

			context.response.write({
				output: JSON.stringify(resultJsonObj)
			})
		}

		function _post(context){
			// create a new HTS record from the posted JSON data
			var postedData = context.data;

			try{

				var newHtsRec = record.create({
					type: record.Type.customrecord_hts
				});

				// map each key on posted data to the custom fields in HTS record
				for (var key in postedData){
					if(postedData.hasOwnProperty(key)){
						newHtsRec.setValue({
							fieldId: key,
							value: postedData[key]
						});
					}
				}

				var newHtsRecId = newHtsRec.save();

			} catch(e){

				var errorObj = {
					error: e.name + ' | ' + e.message
				}
				
				context.response.write({
					output: JSON.stringify(errorObj)
				});
			}

		}

		return{
			get: _get,
			post: _post
		}
	})