# netsuite-coding-challenge

## Getting Started
1) Fork this project
2) Clone your fork
3) Install project dependencies
3) Commit and push code challenges
4) Open a merge request for review

## Install Dependencies
Install the suitecloud cli and unit testing tools using node/npm:

`npm install`

## Challenges

### Client Script
Create a client script deployed on the inventory transfer record that validates the line item; validate the quantity being transferred is available at the location being transferred from. This is needed so that inventory committed on existing orders is not stolen by the transfer. Please include both the script file, and the xml representation of the script deployment customizations. If you can accomplish importing of the xml using SDF, that is preferred.

### User Event Script
Create a user event script deployed on the item fulfillment record that transforms the fulfillment to an invoice. The invoice should be created only for the items and quantity on the fulfillment, not for the full sales order. This is needed so that invoices can be sent to customers as soon as product ships. Please include both the script file, and the xml representation of the script deployment customizations. If you can accomplish importing of the xml using SDF, that is preferred.

### Scheduled Script
Create a scheduled script that submits a saved search task and saves the results of the search to the file cabinet. The script should be parameterized so that the saved search can be selected on the script deployment. This is needed because some saved search results are too large to be returned to the UI. The search must be run in the background on a schedule for a user to pick up from the file cabinet later. Please include both the script file, and the xml representation of the script deployment customizations. If you can accomplish importing of the xml using SDF, that is preferred.

### Map/Reduce
Create a map/reduce script that can pick up a csv file from the file cabinet (from previous challenge). It should sum the results of one of the csv columns, and log that sum to a debug log. Please include both the script file, and the xml representation of the script deployment customizations. If you can accomplish importing of the xml using SDF, that is preferred.

### Custom Record
Keeco imports many goods and must pay tariffs on those goods. In order to comply with trade regulations, Keeco must track the Harmonized Tariff Schedule (HTS) data associated with it's imported goods. Create a custom record based on the latest tariff sample data [here](https://dataweb.usitc.gov/tariff/annual). Not all columns are needed. Select what seems most relevant to you based on your common sense. Please include the xml representation of the script deployment customizations. If you can accomplish importing of the xml using SDF, that is preferred.

### Restlet
Create a restlet that can get and set one of the custom HTS records you created as JSON. Please include both the script file, and the xml representation of the script deployment customizations. If you can accomplish importing of the xml using SDF, that is preferred.
